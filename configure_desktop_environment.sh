#!/bin/bash
#
# BASH script to configure my preferred Gnome theme and settings
#
# Change log:
#  - 2019-04-21: Updated for Ubuntu 19.04
#  - 2020-04-21: Updated for Ubuntu 20.04
#

# Install Gnome-shell extensions, User Themes, Dash to Panel, and Workspace Matrix
notify-send "Install Gnome-shell extensions, User Themes, Dash to Panel, and Workspace Matrix"
echo "Install Gnome-shell extensions, User Themes, Dash to Panel, and Workspace Matrix"
google-chrome 'https://extensions.gnome.org/'
read -p "Press [Enter] key after installing Gnome-shell extensions, User Themes, Dash to Panel, and Workspace Matrix"

# Install Windows 10 theme
#wget https://raw.githubusercontent.com/B00merang-Project/Shell-Scripts/master/auto_install.sh
#chmod u+x auto_install.sh
#./auto_install.sh
pushd /tmp
mkdir windows-10
cd windows-10
wget https://github.com/B00merang-Project/Windows-10/archive/master.zip -q --show-progress
unzip -qq master.zip
if [ ! -d "$HOME/.themes" ]
then
  mkdir "$HOME/.themes"
fi
mv Windows-10-master "$HOME/.themes/Windows 10"
rm -rf /tmp/windows-10
popd

gsettings set org.gnome.desktop.interface gtk-theme 'Windows 10'

# Configure Desktop - Icons
install_win10_icons=false
if [ "$install_win10_icons" = true ] ; then
	mkdir ~/.icons
	pushd ~/.icons
	wget https://github.com/B00merang-Project/Windows-10-icons/archive/master.zip
	unzip master.zip
	mv Windows-10-master/ 'Windows 10'
	popd
	#gsettings set org.gnome.desktop.interface icon-theme 'Windows 10'
fi

# Copy SEGOEUI.TTF file from C:\Program Files (x86)\Microsoft Visual Studio\Installer\resources\app\renderer\fonts to ~/.fonts
mkdir -p ~/.fonts
cp -n SEGOEUI.TTF ~/.fonts
sudo fc-cache -fv

# Use gnome-tweaks to fix settings, or
# If using settings, you can see what's going on by: dconf watch /
#gsettings set org.gnome.desktop.interface icon-theme 'Adawaita'
gsettings set org.gnome.desktop.wm.preferences button-layout ":minimize,maximize,close"
gsettings set org.gnome.desktop.interface gtk-theme 'Windows 10'
#gsettings set org.gnome.desktop.interface cursor-theme 'win8'
#gsettings set org.gnome.desktop.interface icon-theme 'Windows 10'
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas set org.gnome.shell.extensions.dash-to-panel panel-size 32
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas set org.gnome.shell.extensions.dash-to-panel group-apps false
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas set org.gnome.shell.extensions.dash-to-panel group-apps-label-max-width 120
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas set org.gnome.shell.extensions.dash-to-panel group-apps-label-font-size 13
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas set org.gnome.shell.extensions.dash-to-panel show-window-previews false
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas set org.gnome.shell.extensions.dash-to-panel isolate-workspaces true
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas set org.gnome.shell.extensions.dash-to-panel isolate-monitors true
gsettings --schemadir ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas set org.gnome.shell.extensions.dash-to-panel show-show-apps-button false
gsettings set org.gnome.desktop.interface enable-hot-corners false
dconf write /org/gnome/terminal/legacy/theme-variant "'light'"

# Set favorite apps
gsettings set org.gnome.shell favorite-apps "['org.gnome.Terminal.desktop', 'org.gnome.Nautilus.desktop']"

# Set Thunar as the default file manager
xdg-mime default Thunar.desktop inode/directory

# Make workspaces apply to all monitors
gsettings set org.gnome.shell.overrides workspaces-only-on-primary false
gsettings set org.gnome.mutter workspaces-only-on-primary false

# Fix Gnome Shell theme to use my preferred font
mkdir -p ~/.themes/BensGnomeShell/gnome-shell
echo $'@import url("resource:///org/gnome/theme/gnome-shell.css");\n\n#panel {\n    font-family: "Segoe UI";\n    font-weight: normal;\n}' > ~/.themes/BensGnomeShell/gnome-shell/gnome-shell.css
gsettings --schemadir ~/.local/share/gnome-shell/extensions/user-theme@gnome-shell-extensions.gcampax.github.com/schemas set org.gnome.shell.extensions.user-theme name 'BensGnomeShell'

# Misc Gnome tweaks
gsettings set org.gnome.desktop.interface cursor-theme 'Adwaita'
gsettings set org.gnome.desktop.background show-desktop-icons false

# Set background color to grey
gsettings set org.gnome.desktop.background picture-options 'none'
gsettings set org.gnome.desktop.background color-shading-type "solid"
gsettings set org.gnome.desktop.background primary-color "#3d3d3d"
gsettings set org.gnome.desktop.background secondary-color "#3d3d3d"

echo "Download KeepPassRPC and KeeAgent and exit browser"
firefox -new-tab -url https://github.com/kee-org/keepassrpc/releases -new-tab -url https://lechnology.com/software/keeagent/#download
read -p "Press [Enter] key after downloading plugins.  You will want to copy the plgx files to /usr/lib/keepass2/Plugins"

echo "Download Firefox plugins and exit browser"
firefox -new-tab -url https://addons.mozilla.org/en-US/firefox/addon/keefox/ -new-tab -url https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/ -new-tab -url https://addons.mozilla.org/en-US/firefox/addon/noscript/ -new-tab -url https://addons.mozilla.org/en-US/firefox/addon/the-camelizer-price-history-ch/
read -p "Press [Enter] key after downloading plugins."
