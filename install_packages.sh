#!/bin/bash
#
# BASH script to configure my preferred Gnome theme and settings
#
# Change log:
#  - 2021-09-03: Updated for Ubuntu 21.04
#

sudo apt-get update

sudo apt-get dist-upgrade

sudo apt-get install -y openssh-server git build-essential cmake vim htop tmux net-tools sox tigervnc-viewer alacarte gnome-session chrome-gnome-shell gnome-tweaks keepass2 mono-complete curl jq lm-sensors htop software-properties-common gimp inkscape thunar vlc calibre samba filezilla remmina libgconf-2-4 libcanberra-gtk-module lua-rex-pcre python3-venv python3-pip keepass2 mono-complete

# Pretty MS fonts
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections
sudo apt-get install -y ttf-mscorefonts-installer


# Wireshark
echo "wireshark-common wireshark-common/install-setuid boolean true" | sudo debconf-set-selections
sudo apt-get install -y tshark wireshark

# Useful Python stuff
sudo pip3 install numpy scipy matplotlib

# VM components
sudo apt-get install -y qemu-kvm libvirt-dev bridge-utils virt-manager virt-viewer libguestfs-tools

# Sabnzbplus
sudo add-apt-repository -y ppa:jcfp/nobetas
sudo apt-get -y update && sudo apt-get -y dist-upgrade
sudo apt-get install -y sabnzbdplus python-sabyenc

# Chrome
pushd /tmp
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
popd
sudo apt-get install chrome-gnome-shell

# NVidia Driver
# Check https://help.ubuntu.com/community/BinaryDriverHowto/Nvidia for latest driver
install_nvidia_driver=false
if [ "$install_nvidia_driver" = true ] ; then
	sudo apt-get install nvidia-driver-470
fi

# KeePass2
install_keepass_plugins=false
if [ "$install_keepass_plugins" = true ] ; then
    sudo apt-add-repository ppa:dlech/keepass2-plugins-beta
    sudo apt-get update
    sudo apt-get install keepass2-plugin-tray-icon

    #sudo apt-add-repository -y ppa:dlech/keepass2-plugins
    #sudo apt-get -y update && sudo apt-get dist-upgrade
    #sudo apt-get -y install keepass2-plugin-tray-icon
    #sudo apt-get -y install keepass2-plugin-libunity
    #sudo apt-get -y install keepass2-plugin-status-notifier
    #sudo apt-get -y install keepass2-plugin-keeagent
    #sudo apt-get -y install keepass2-plugin-rpc
fi
