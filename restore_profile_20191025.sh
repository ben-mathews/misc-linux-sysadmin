#!/bin/bash

SOURCE_DIR=~/profile_backup

# Home directory
cp -rn $SOURCE_DIR/home /
rm -rf ~/.mozilla
cp -r $SOURCE_DIR/home/ben/.mozilla ~/

# /etc directory
echo "Manually update the smb.conf file"

# Misc etc
sudo cp -a $SOURCE_DIR/etc/ssh/sshd_config /etc/ssh/
sudo cp -a $SOURCE_DIR/etc/samba/smb.conf /etc/samba

# libvirt
sudo cp -rn $SOURCE_DIR/etc/libvirt/* /etc/libvirt

sudo cp -a $SOURCE_DIR/etc/libvirt/qemu/*.xml /etc/libvirt/qemu/
sudo cp -a $SOURCE_DIR/etc/libvirt/qemu/autostart/*.xml /etc/libvirt/qemu/autostart/ 2>/dev/null
sudo cp -a $SOURCE_DIR/etc/libvirt/qemu/networks/*.xml /etc/libvirt/qemu/networks/
sudo cp -a $SOURCE_DIR/etc/libvirt/qemu/networks/autostart/*.xml /etc/libvirt/qemu/networks/autostart/ 2>/dev/null

sudo cp -a $SOURCE_DIR/etc/libvirt/storage/*.xml /etc/libvirt/storage/
sudo cp -a $SOURCE_DIR/etc/libvirt/storage/autostart/*.xml /etc/libvirt/storage/autostart/

echo "Link images folder with \"sudo mv images/ images_orig; sudo ln -s /mnt/VM_Storage/var/lib/libvirt/images/ /var/lib/libvirt\""


# /usr directory
sudo cp -a $SOURCE_DIR/usr/lib/keepass2/Plugins/* /usr/lib/keepass2/Plugins/

# Create directories
sudo mkdir /media/ShareRW
sudo chown -R ben:ben /media/ShareRW/
chmod a+rw /media/ShareRW/

