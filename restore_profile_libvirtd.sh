#!/bin/bash

SOURCE_DIR=/media/ben/VM_Storage/BackUp/20200423/profile_backup

# libvirt
sudo cp -rn $SOURCE_DIR/etc/libvirt/* /etc/libvirt

sudo cp -a $SOURCE_DIR/etc/libvirt/qemu/*.xml /etc/libvirt/qemu/
sudo cp -a $SOURCE_DIR/etc/libvirt/qemu/autostart/*.xml /etc/libvirt/qemu/autostart/ 2>/dev/null
sudo cp -a $SOURCE_DIR/etc/libvirt/qemu/networks/*.xml /etc/libvirt/qemu/networks/
sudo cp -a $SOURCE_DIR/etc/libvirt/qemu/networks/autostart/*.xml /etc/libvirt/qemu/networks/autostart/ 2>/dev/null

sudo cp -a $SOURCE_DIR/etc/libvirt/storage/*.xml /etc/libvirt/storage/
sudo cp -a $SOURCE_DIR/etc/libvirt/storage/autostart/*.xml /etc/libvirt/storage/autostart/

echo "Link images folder with \"sudo mv images/ images_orig; sudo ln -s /mnt/VM_Storage/var/lib/libvirt/images/ /var/lib/libvirt\""

