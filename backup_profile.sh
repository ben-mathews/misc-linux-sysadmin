#!/bin/bash

#
# Run full backup with:
#   sudo tar czvf /mnt/VM_Storage/BackUp/20211123/full_backup_20211123.tar.gz --exclude=/dev --exclude=/lost+found --exclude=/media --exclude=/mnt --exclude=/usr/local/MATLAB --exclude=/usr/local/lib --exclude=/usr/local/share --exclude=/usr/local/bin --exclude=/usr/lib --exclude=/usr/share --exclude=/usr/bin --exclude=/usr/src --exclude=/usr/include --exclude=/proc --exclude=/run --exclude=/snap --exclude=/swapfile --exclude=/sys --exclude=/tmp --exclude=/var/log --exclude=/var/cache /
#

DEST_DIR=~/profile_backup
DEST_DIR=/mnt/VM_Storage/BackUp/20211123/profile_backup
#
# Profile -----------------------------------------------------------------------------------------
#

mkdir $DEST_DIR
mkdir -p $DEST_DIR$HOME
mkdir $DEST_DIR$HOME/.config
mkdir $DEST_DIR$HOME/.config/menus
mkdir $DEST_DIR$HOME/.config/google-chrome
mkdir $DEST_DIR$HOME/.local
mkdir $DEST_DIR$HOME/.local/share
mkdir $DEST_DIR$HOME/.local/share/icons

# General
cp -r ~/Profile $DEST_DIR$HOME/
cp -r ~/.fonts $DEST_DIR$HOME/
cp -r ~/.gitconfig $DEST_DIR$HOME/
cp -r ~/.icons $DEST_DIR$HOME/
cp -r ~/.ssh $DEST_DIR$HOME/

# Data
cp -r ~/Documents $DEST_DIR$HOME/
cp -r ~/Downloads $DEST_DIR$HOME/
cp -r ~/Pictures $DEST_DIR$HOME/
cp -r ~/Documents $DEST_DIR$HOME/
cp -r ~/dev $DEST_DIR$HOME/

# Remmina
cp -r ~/.local/share/remmina $DEST_DIR$HOME/.local/share
cp -r ~/.config/remmina $DEST_DIR$HOME/.config/remmina
cp -r ~/.config/freerdp $DEST_DIR$HOME/.config/freerdp

# Filezilla
cp -r ~/.config/filezilla $DEST_DIR$HOME/.config/filezilla

# Calibre
cp -r ~/.config/calibre $DEST_DIR$HOME/.config/calibre

# Transmission
cp -r ~/.config/transmission $DEST_DIR$HOME/.config/transmission

# Sabnzbd
cp -r ~/.sabnzbd $DEST_DIR$HOME/

# Firefox
cp -r ~/.mozilla $DEST_DIR$HOME/

# Chrome
cp -r ~/.config/google-chrome $DEST_DIR$HOME/.config/google-chrome


#
# System ------------------------------------------------------------------------------------------
# 
mkdir $DEST_DIR/etc
mkdir $DEST_DIR/etc/ssh
mkdir $DEST_DIR/etc/libvirt
mkdir $DEST_DIR/etc/libvirt/qemu
mkdir $DEST_DIR/etc/libvirt/qemu/autostart
mkdir $DEST_DIR/etc/libvirt/qemu/networks
mkdir $DEST_DIR/etc/libvirt/qemu/networks/autostart
mkdir $DEST_DIR/etc/libvirt/storage
mkdir $DEST_DIR/etc/libvirt/storage/autostart
mkdir $DEST_DIR/usr
mkdir $DEST_DIR/usr/lib
mkdir $DEST_DIR/usr/lib/keepass2
#mkdir $DEST_DIR/usr/lib/keepass2/plugins
mkdir $DEST_DIR/usr/lib/keepass2/Plugins
mkdir $DEST_DIR/etc/samba

cp -a /etc/fstab $DEST_DIR/etc/
cp -a /etc/ssh/sshd_config $DEST_DIR/etc/ssh

cp -a /etc/samba/smb.conf $DEST_DIR/etc/samba

cp -a /usr/lib/keepass2/Plugins/* $DEST_DIR/usr/lib/keepass2/Plugins

# libvirtd
sudo cp -a /etc/libvirt/qemu/*.xml $DEST_DIR/etc/libvirt/qemu/
sudo cp -a /etc/libvirt/qemu/autostart/*.xml $DEST_DIR/etc/libvirt/qemu/autostart/ 2>/dev/null
sudo cp -a /etc/libvirt/qemu/networks/*.xml $DEST_DIR/etc/libvirt/qemu/networks/
sudo cp -a /etc/libvirt/qemu/networks/autostart/*.xml $DEST_DIR/etc/libvirt/qemu/networks/autostart/ 2>/dev/null
sudo cp -a /etc/libvirt/qemu/*.xml $DEST_DIR/etc/libvirt/qemu/
sudo cp -a /etc/libvirt/storage/*.xml $DEST_DIR/etc/libvirt/storage
sudo cp -a /etc/libvirt/storage/autostart/*.xml $DEST_DIR/etc/libvirt/storage/autostart/ 2>/dev/null

echo "Copy with \"sudo rsync -avz -e ssh $DEST_DIR ben@192.168.122.39:~/\" (replace paths and IP addresses as necessary)"
